﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UP
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double Y = 0;
            double X = Convert.ToDouble(tb2.Text);

            if (-7 <= X && X < -3)
            {
                double R = Convert.ToDouble(tb1.Text);
                Y = 3;
            }
            else if (-3 <= X && X <= 3)
            {
                double R = Convert.ToDouble(tb1.Text);
                Y = -Math.Sqrt(Math.Pow(R, 2) - Math.Pow(X, 2)) + 3;
            }
            else if (3 < X && X < 6)
            {
                double R = Convert.ToDouble(tb1.Text);
                Y = -2 * X + Math.Pow(R, 2);
            }
            else
            {
                Y = X - 9;
            }

            tb3.Text = "Значение ординаты : " + Y;
        }

        Form1 F1;
        private void btn2_Click(object sender, EventArgs e) 
        { 
 
            F1 = new Form1();
            F1.Show();
            Hide();
            
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
