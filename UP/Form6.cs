﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UP
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }

        private void Form6_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        Form1 F1;
        private void btn1_Click(object sender, EventArgs e)
        {
            F1 = new Form1();
            F1.Show();
            Hide();
        }


        private void btn2_Click(object sender, EventArgs e)
        {
            try
            {

                int n = Convert.ToInt32(tb1.Text);
                int[] array = new int[n];
                Random rand = new Random();
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = rand.Next(10);
                    tb2.Text = tb2.Text + " " + array[i] + " ";
                }
                int max = array[0];
                int index = 0;
                for (int i = 0; i < array.Length; i++)
                {
                    if (array[i] > max)
                    {
                        max = array[i];
                        index = i;
                    }
                }
                tb3.Text = Convert.ToString(max);

                int zero1 = Array.IndexOf(array, 0), zero2 = Array.IndexOf(array, 0, zero1 + 1);
                if (zero1 == -1 || zero2 == -1)
                    MessageBox.Show("Недостаточно нулей");
                else
                    tb4.Text = Convert.ToString(+new ArraySegment<int>(array, zero1 + 1, zero2 - zero1 - 1).Aggregate(1, (a, b) => a * b));

                // переставляем значения
                int tmp;
                for (int i = 0; i < array.Length / 2; i++)
                {
                    if (i % 2 != 0)
                    {
                        tmp = array[i + array.Length / 2];
                        array[i + array.Length / 2] = array[i];
                        array[i] = tmp;
                    }
                }

                // выводим в label2 для наглядности
                foreach (int i in array)
                {
                    lbl6.Text = lbl6.Text + i.ToString() + " ";
                }
            }
            catch
            {
                MessageBox.Show("Ошибка, попробуйте еще раз");
            }

        }



        private void bt3_Click(object sender, EventArgs e)
        {
 
        }
    }
}
